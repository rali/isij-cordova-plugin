package cordova.plugin.isij;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.IOException;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;



public class SalaatTimesProvider {

    private JSONArray salaatTimesArray;

    public SalaatTimesProvider(InputStream jsonInputStream) {
        this.parseJSON(this.preloadJSONFile(jsonInputStream));
    }

    public AdhaanTime getUpcomingAdhaan(Date afterDateTime) {
        return this.findUpcomingAdhaanTime(0, afterDateTime);
    }

    private Calendar generateSalaatTime(Date refTime, int i, int hour, int minute) {
      Calendar salaatTimeCal = GregorianCalendar.getInstance();
                  salaatTimeCal.setTime(refTime);
                  salaatTimeCal.set(GregorianCalendar.SECOND, 0);
                  salaatTimeCal.set(GregorianCalendar.HOUR, hour);
                  salaatTimeCal.set(GregorianCalendar.MINUTE, minute);

                  if (i > 3) {
                    salaatTimeCal.set(Calendar.AM_PM, Calendar.PM);
                  } else {
                    salaatTimeCal.set(Calendar.AM_PM, Calendar.AM);
                  }

      return salaatTimeCal;
    }

    private AdhaanTime findUpcomingAdhaanTime(int dayAppend, Date afterDateTime) {
        Calendar now = GregorianCalendar.getInstance();


        now.add(GregorianCalendar.DAY_OF_MONTH, dayAppend);

        int hourNum = now.get(GregorianCalendar.HOUR_OF_DAY);
          int dayNum = now.get(GregorianCalendar.DAY_OF_MONTH) - 1;
          int monthNum = now.get(GregorianCalendar.MONTH);

        if (dayAppend > 0) {
          now.set(GregorianCalendar.HOUR_OF_DAY, 0);
        }

        now.set(Calendar.SECOND, 0);

        String adhaanPeriod = "Fajr";

        try {
            JSONArray todaysTimes = salaatTimesArray.getJSONArray(monthNum).getJSONArray(dayNum);
            Date nextSalaatTime = null;

            //Figure out current time
            for (int i = 0; i < todaysTimes.length(); i++) {


                // Skip first two (Imsaak, Sunrise, Sunset)
                if (i == 0 || i == 2 || i == 4) {
                    continue;
                }

                if (i == 1) {
                  adhaanPeriod = "Fajr";
                } else if (i == 3) {
                  adhaanPeriod = "Zohr";
                } else if (i == 5) {
                  adhaanPeriod = "Maghrib";
                }

                  Date currenTime = now.getTime();
                  String[] salaatTimeArray = todaysTimes.getString(i).split(":");

                  Calendar salaatTimeCal = generateSalaatTime(currenTime, i, Integer.parseInt(salaatTimeArray[0]),
                    Integer.parseInt(salaatTimeArray[1]));


                   if (i > 2) {
                    salaatTimeCal.set(Calendar.AM_PM, Calendar.PM);
                  } else {
                    salaatTimeCal.set(Calendar.AM_PM, Calendar.AM);
                  }

                   // System.out.println(salaatTimeArray[0]+":"+salaatTimeArray[1]);
                   // System.out.println(i);
                   // System.out.println(currenTime);
                   // System.out.println(afterDateTime );
                   // System.out.println(salaatTimeCal.getTime());
                   // System.out.println("-----");

                  //Check if current time falls after salaat time
                  //If so this means that we are nearing end of date
                  //Set current time to beginning of next day
                  if (i == 5  && currenTime.after(salaatTimeCal.getTime())) {
                    //  System.out.println("ok");
                    // now.add(GregorianCalendar.DAY_OF_MONTH, 1);
                    // now.set(GregorianCalendar.HOUR_OF_DAY, 0);
                    // now.set(GregorianCalendar.MINUTE, 0);

                    // currenTime = now.getTime();
                    // salaatTimeCal = generateSalaatTime(currenTime, i,  Integer.parseInt(salaatTimeArray[0]),
                    // Integer.parseInt(salaatTimeArray[1]));
                    // return new AdhaanTime(salaatTimeCal.getTime(), adhaanPeriod);
                    return findUpcomingAdhaanTime(dayAppend + 1, afterDateTime);
                  }


                  if (currenTime.before(salaatTimeCal.getTime()) && afterDateTime == null) {

                        nextSalaatTime = salaatTimeCal.getTime();
                        break;

                  }  else if (afterDateTime != null) {


                             //Create a slight time different between salaat time and skip time
                             Calendar atCal = GregorianCalendar.getInstance();
                             atCal.setTime(afterDateTime);
                             atCal.add(GregorianCalendar.SECOND, 30);



                        if (salaatTimeCal.getTime().after(atCal.getTime())) {

                            nextSalaatTime = salaatTimeCal.getTime();
                            break;
                        }
                  }
            }

            if (nextSalaatTime != null) {
                return new AdhaanTime(nextSalaatTime, adhaanPeriod);
            } else {

                return findUpcomingAdhaanTime(dayAppend + 1, afterDateTime);
            }

        } catch (Exception e) {
          e.printStackTrace();
            return null;
        }
    }

    private String preloadJSONFile(InputStream is) {
        String json = null;
        try {
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void parseJSON(String json) {
        try {
            this.salaatTimesArray = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
