package cordova.plugin.isij;

import java.util.Date;

class AdhaanTime {
    private Date date;
    private String period;
    public AdhaanTime(Date date, String period) {
        this.date = date;
        this.period = period;
    }

    public Date getDate() {
        return this.date;
    }

    public String getPeriod() {
        return this.period;
    }
}
